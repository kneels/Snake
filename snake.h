#include <stdlib.h>
#include <malloc.h>
#include "Kneels_PCD8544.h"

#define MAX_SNAKE_LEN 50	// maximum length of the snake
#define START_SNAKE_LEN 5 	// starting length of the snake
#define SNAKE_WIDTH 2
#define HEAD snakePos[0]
#define SEG_SIZE 2			// size of one snake segment

/* Dimensions of the level */
#define LVL_X_MIN 1
#define LVL_X_MAX 83
#define LVL_Y_MIN 1
#define LVL_Y_MAX 47

typedef struct {
	uint8_t x;
	uint8_t y;
} Point;

typedef enum {
	UP,
	RIGHT,
	DOWN,
	LEFT
} Direction;

Direction direction;
boolean gameOver = false;
uint16_t score;
uint16_t speed = 160;
uint8_t snakeLen;							// snake length
Point food;									// position of the food
Point *snakePos[MAX_SNAKE_LEN];				// position of the snake

/* Prototypes */
void setSegment(Point *p, boolean on);
void drawSnake();
void spawnFood();
void snakeInit();
boolean inSnakeArray(Point *p);
void clearSnake();
void endGame();
void detectCollision();
void moveSnake(Direction d);
void drawBorder();

void drawBorder() {
	int i;
	// top
	for(i = 0; i < LCD_X; i++) {
		setPixel(i, 0, HIGH);	
	}
	// right
	for(i = 0; i < LCD_Y; i++) {
		setPixel(LVL_X_MAX, i, HIGH);	
	}
	// bottom
	for(i = 0; i < LCD_X; i++) {
		setPixel(i, LVL_Y_MAX, HIGH);	
	}
	// left
	for(i = 0; i < LCD_Y; i++) {
		setPixel(0, i, HIGH);	
	}
}

void setSegment(Point *p, boolean on) {
	setPixel(p->x, p->y, on);
	setPixel(p->x+1, p->y, on);
	setPixel(p->x, p->y+1, on);
	setPixel(p->x+1, p->y+1, on);
}

void drawSnake() {	
	int i;
	for (i = 0; i < snakeLen; i++) {
		setSegment(snakePos[i], HIGH);
	}
}

void snakeInit() {
	snakeLen = START_SNAKE_LEN;
	direction = RIGHT;
	score = 0;

	int i, tempX;
	tempX = LVL_X_MAX / 2;
	for(i = 0; i < snakeLen; i++) {
		snakePos[i] = (Point*) malloc(sizeof(Point));
		if(!snakePos[i]) {
			exit(1);
		}
		snakePos[i]->x = tempX;
		snakePos[i]->y = LVL_Y_MAX / 2;
		tempX -= SEG_SIZE;
	}
	drawSnake();
	spawnFood();
}

void spawnFood() {	
	do {
		randomSeed(analogRead(0));
		food.x = (uint8_t) random(LVL_X_MIN, LVL_X_MAX - SEG_SIZE) | 0x01;
		food.y = (uint8_t) random(LVL_Y_MIN, LVL_Y_MAX - SEG_SIZE) | 0x01;
	} while(inSnakeArray(&food));

	setSegment(&food, HIGH);
}

boolean inSnakeArray(Point *p) {
	int i;
	for(i = 0; i < snakeLen; i++) {
		if(snakePos[i]->x == p->x && snakePos[i]->y == p->y) {
			return true;
		}
	}
	return false;
}

void clearSnake() {
	int i;
	for(i = 0; i < snakeLen; i++) {
		setSegment(snakePos[i], LOW);
		free(snakePos[i]);
	}
}

void endGame() {
	char scoreBuffer[4];
	sprintf(scoreBuffer, "%d", score);
	clearSnake();
	lcdClear();
	gotoXY(12,1);
	lcdWriteString("GAME OVER");
	gotoXY(12,2);
	lcdWriteString("Score: ");
	lcdWriteString(scoreBuffer);
	delay(5000);
	lcdInit();
	drawBorder();
	snakeInit();
}

void detectCollision() {
	uint8_t sX, sY, fX, fY;
	sX = HEAD->x;
	sY = HEAD->y;
	fX = food.x;
	fY = food.y;

	if(sX == fX && sY == fY) {				// ran into food 
		if(snakeLen == MAX_SNAKE_LEN) {
			return;
		}

		// add segment						
		snakePos[snakeLen] = (Point*) malloc(sizeof(Point));
		if(!snakePos[snakeLen]) {
			exit(1);
		}
		snakePos[snakeLen]->x = snakePos[snakeLen-1]->x;
		snakePos[snakeLen]->y = snakePos[snakeLen-1]->y;
		score++;
		snakeLen++;
		spawnFood();
	}
	else if(checkPixel(HEAD->x, HEAD->y)) {
		// ran into self
		endGame();
	}
}

void moveSnake(Direction d) {
	int i;
	direction = d;
	// erase tail
	setSegment(snakePos[snakeLen-1], LOW);

	// move segments one position up
	for(i = snakeLen - 1; i > 0; i--) {
		snakePos[i]->x = snakePos[i - 1]->x;
		snakePos[i]->y = snakePos[i - 1]->y;
	}

	// set new snake head position
	switch (direction) {
	    case RIGHT:
	      if(HEAD->x < LVL_X_MAX - SEG_SIZE) {
	      	HEAD->x += SEG_SIZE;
	      } else {
	      	HEAD->x = LVL_X_MIN;
	      }
	      break;
	    case UP:
	      if(HEAD->y > LVL_Y_MIN) {
	      	HEAD->y -= SEG_SIZE;
	      } else {
	      	HEAD->y = LVL_Y_MAX - SEG_SIZE;
	      }
	      break;
	    case DOWN:
 		  if(HEAD->y < LVL_Y_MAX - SEG_SIZE) {
	      	HEAD->y += SEG_SIZE;
	      } else {
	      	HEAD->y = LVL_Y_MIN;	
	      }
	      break;
	    case LEFT:
	      if(HEAD->x > LVL_X_MIN) {
	      	HEAD->x -= SEG_SIZE;
	      } else {
	      	HEAD->x = LVL_X_MAX - SEG_SIZE;
	      }
	      break;
	}	
	detectCollision();
	drawSnake();
}