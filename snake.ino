#include "snake.h"

#define PIN_UP 7
#define PIN_RIGHT 6
#define PIN_DOWN 5
#define PIN_LEFT 4

long previousMillis = 0;
int incomingByte = 0;

void setup() {
	pinMode(PIN_UP, INPUT);
	pinMode(PIN_RIGHT, INPUT);
	pinMode(PIN_DOWN, INPUT);
	pinMode(PIN_LEFT, INPUT);

	lcdInit();
	drawBorder();
	snakeInit();
}

void loop() {
	unsigned long currentMillis = millis();
	if(currentMillis - previousMillis > speed) {
    	previousMillis = currentMillis;   
		moveSnake(direction);
	}

	if(digitalRead(PIN_UP)) {
		if(direction != DOWN && direction != UP) {
			moveSnake(UP);
		}
	} 
	else if(digitalRead(PIN_RIGHT)) {
		if(direction != LEFT && direction != RIGHT) {
			moveSnake(RIGHT);
		}
	}
	else if(digitalRead(PIN_DOWN)) {
		if(direction != UP && direction != DOWN) {
			moveSnake(DOWN);
		}
	}
	else if(digitalRead(PIN_LEFT)) {
		if(direction != RIGHT && direction != LEFT) {
			moveSnake(LEFT);
		}
	}
}