Snake on Arduino
========
I always liked the Snake games on the old Nokia phones, so I decided to make my own for the Arduino using the PCD8544 display. It requires my [PCD8544 Library](https://github.com/kneels/Kneels-PCD8544-Library) to run.

![alt tag](http://i.imgur.com/ZvXvlAr.jpg)
